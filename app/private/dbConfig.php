<?php

//environment
define('PROD_DB', 0);

if (PROD_DB) {
    //Production database information
    define('HOST', '');
    define('USERNAME', '');
    define('PASSWORD', '');
    define('DATABASE', '');
    define('DBTABLE', '');
} else {
    //Development database information
    define('HOST', '192.168.33.10');
    define('USERNAME', 'user');
    define('PASSWORD', 'password');
    define('DATABASE', 'necVoter');
    define('DBTABLE', 'voter');
}