<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);

/**
 * Description of necLibera
 *
 * @author riomansaray
 */
require_once('dbConfig.php');

class necApp {

    private $connect;
    private $numOfRows;
    public $voterId;
    public $voterName;
    public $voterCounty;
    public $voterMagisterialArea;
    public $electoralDistrict;
    public $votingPrecinct;
    public $pollingPace;

    private function dbConnect() {

        $this->connect = new mysqli(HOST, USERNAME, PASSWORD, DATABASE);

        //check connection
        if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        }

        return $this->connect;
    }

    private function queryDatabase($voterId) {

        $stmt = $this->dbConnect()->prepare("SELECT "
                . "voterId, voterName,"
                . "county, magisterialArea, electoralDistrict,"
                . " votingPrecinct, pollingPlace  "
                . "FROM " . DBTABLE . " WHERE voterId=? LIMIT 1");


        if (!$stmt) {
            die('Prepare failed: ' . $this->connect->error);
        }

        //bind parameters for markers
        $stmt->bind_param("s", $voterId);

        //execute query
        $stmt->execute();

        //Store the result (to get properties)
        $stmt->store_result();

        //Get the number of rows
        $this->numOfRows = $stmt->num_rows;

        return $stmt;
    }

    public function getVoterDetails($voterId) {

        if (!$voterId || !is_numeric($voterId)) {
            return;
        }

        $stmt = $this->queryDatabase($voterId);

        //Bind the result to variables
        $stmt->bind_result(
                $voterId, $voterName, $county, $magisterialArea, $electoralDistrict, $votingPrecinct, $pollingPlace
        );

        if ($this->numOfRows > 0) {

            while ($stmt->fetch()) {
                $this->voterId = $voterId;
                $this->voterName = $voterName;
                $this->voterCounty = $county;
                $this->voterMagisterialArea = $magisterialArea;
                $this->electoralDistrict = $electoralDistrict;
                $this->votingPrecinct = $votingPrecinct;
                $this->pollingPace = $pollingPlace;
                $_SESSION['id'] = $this->voterId = $voterId;
                include_once '../public/modalHeader.php'; //Modal header
                include_once '../public/modalFooter.php'; //Modal footer
            }
        }

        //free results
        $stmt->free_result();

        //close statement
        $stmt->close();
    }

    public function saveVoterDetails() {

    }

}

$app = new necApp();
$app->getVoterDetails(123456789);
