function necVoterStatus() {

    this.isSuccess = {
        success: true,
        message: ''
    };

    this.voterID = $('.voterId').val().trim();
    this.getVoterDetailsUrl = '';
    this.voterIDCheckURL = 'http://rio.dev/necliberia/app/public/voterDetails.php';

}

necVoterStatus.prototype.checkInputFiled = function () {
    if (this.voterID === '')
    {
        this.isSuccess.success = false;
        this.isSuccess.message = 'Voter ID cannot be empty. Please correct and try again!';
        return;
    }
};

necVoterStatus.prototype.googleCaptchCheck = function () {
    if (grecaptcha.getResponse() === '')
    {
        this.isSuccess.success = false;
        this.isSuccess.message = 'Please complete the Captcha!';
        return;
    }
};

necVoterStatus.prototype.checkVoterIDLenght = function () {

    if (!this.voterID) {
        return;
    }

    //Check the voter id lenght.
    if (this.voterID.length !== 9)
    {
        this.isSuccess.success = false;
        this.isSuccess.message = 'Voter ID must be 9 digits long. Please correct and try again!';
        return;
    }

};


necVoterStatus.prototype.showMessage = function () {
    if (this.isSuccess.success === false)
    {
        $('#errorModal').modal({backdrop: 'static', keyboard: false});
        $('.errorText').html(this.isSuccess.message);
    }

};

//gets the voter details.
necVoterStatus.prototype.getVoterDetails = function () {


    if (this.isSuccess.success === false)
    {
        return;
    }

    $.ajax({
        type: 'GET',
        url: this.voterIDCheckURL,
        data: {'id': this.voterID}
    }).done(function (data) {

        if (data) {
            $('.showVoterDetails').html(data);
            $('.exampleModal').modal({backdrop: 'static', keyboard: false}); // show the voter details in the modal
            $('.voterId').val(''); //Reset Voter ID text file.
            grecaptcha.reset(); //Reset google Captcha.
        } else {
            var errortext = 'Your current electoral enrolment could not be confirmed using the details you entered. ' +
                    'Please contact the NEC on 13 23 26 for assistance.';

            $('#errorModal').modal({backdrop: 'static', keyboard: false});
            $('.errorText').html(errortext);
        }

    });

};


necVoterStatus.prototype.doValidation = function () {
    this.googleCaptchCheck();
    this.checkInputFiled();
    this.checkVoterIDLenght();
    this.showMessage();
};

$(document).ready(function () {

//Prevents the user from entering any special characters.
    $('.voterId').on('input', function (event) {
        this.value = this.value.replace(/[^0-9]/g, '');
        return;
    });

    $('.submit').click(function () {
        var voterStatus = new necVoterStatus();
        voterStatus.doValidation();
        voterStatus.getVoterDetails();
    });

    $('.clear').click(function () {
        $('.voterId').val('');
        //Reset Captcha
        if (grecaptcha.getResponse() !== '') {
            grecaptcha.reset();
        }
    });
});