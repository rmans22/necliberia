<?php

session_start();

ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);

if (is_numeric(filter_input(INPUT_GET, 'id'))) {
    require '../private/necLiberia.php';
    $necApp = new necApp();
    $voaterDetails = $necApp->getVoterDetails(filter_input(INPUT_GET, 'id'));
}