<?php
if (!isset($_SESSION['id'])) {
    return;
}

if (filter_input(INPUT_GET, 'userClicked') === 'dfas34saf44324' && filter_input(INPUT_GET, 'action') === 'logout') {
    session_destroy();
    return;
}
?>

<script>
    //Kill session
    $('.exit').click(function () {

        var exitURL = 'http://rio.dev/necliberia/app/public/modalHeader.php';
        var logOutKey = 'dfas34saf44324';
        var action = 'logout';

        $.ajax({
            type: 'GET',
            url: exitURL,
            data: {'action': action, 'userClicked': logOutKey}
        }).done(function () {
            $('.showVoterDetails').hide();
            location.reload();
            return;
        });
    });
</script>

<div class="modal fade exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">You are enrolled to vote</h5>
                <button type="button" class="close exit" data-dismiss="modal" aria-label="Close" style="cursor: pointer;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-responsive table-bordered table-hover table-striped" style="font-size: 12px;">
                    <tbody>
                        <tr>
                            <th scope="row">Voter ID:</th>
                            <td><?php echo $this->voterId ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Voter Name:</th>
                            <td><?php echo $this->voterName; ?></td>

                        </tr>
                        <tr>
                            <th scope="row">County:</th>
                            <td><?php echo $this->voterCounty; ?></td>

                        </tr>

                        <tr>
                            <th scope="row">Magisterial Area:</th>
                            <td><?php echo $this->voterMagisterialArea; ?></td>
                        </tr>

                        <tr>
                            <th scope="row">Electoral District:</th>
                            <td><?php echo $this->electoralDistrict; ?></td>
                        </tr>

                        <tr>
                            <th scope="row">Voting Precinct:</th>
                            <td><?php echo $this->votingPrecinct; ?></td>

                        </tr>

                        <tr>
                            <th scope="row">Polling Place:</th>
                            <td><?php echo $this->pollingPace; ?></td>
                        </tr>
