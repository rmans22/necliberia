<?php
if (isset($_GET['pageName']) === 'admin') {
    echo 'Load admin page here';
}
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>NEC Liberia :: Check Were You Vote</title>

        <!-- Bootstrap core CSS -->
        <link href="app/public//css/bootstrap.min.css" rel="stylesheet"/>

        <!-- Custom styles for this template -->
        <link href="app/public/css/layout.css" rel="stylesheet">

        <!--Jquery-->
        <script src="app/public/js/jquery.js"></script>

        <!--Custom JS for this template -->
        <script src="app/public/js/app.js"></script>

        <style>
            .modal-body{
                padding: 9px !important;
            }

            table td{
                font-size: 13px !important;
            }

            .btn-danger:hover{
                cursor: pointer !important;
            }

            .errorText{
                color: #4b4f56;
            }
        </style>

    </head>
    <body>

        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="container">

        </div>

        <div class="container">
            <div class="jumbotron">
            </div>

            <div class="form-signin">
                <div class="form-group">
                    <label for="voterId" style="font-size: 14px;">Enter your Voter ID to check where you vote</label>
                    <input type="text" class="form-control voterId input" id="voterId" maxlength="9" autofocus/>
                    <small class="form-text text-muted">Voter ID (9 Digits)</small>
                    <br/>
                    <div class="g-recaptcha hideCaptcha" data-sitekey="6Le1yTIUAAAAAB3VUQq76zJn2QbVux8a1agUGbTQ"></div>

                </div>

                <button type="submit" class="btn btn-primary btn-success submit" style="cursor: pointer;">&nbsp;Check</button>
                <button type="submit" class="btn btn-primary btn btn-danger clear" style="cursor: pointer;">&nbsp;Clear</button>
            </div>

            <div class="showVoterDetails"></div>

        </div> <!-- /container -->

<?php
//Errors modal
include_once './app/public/errorsModal.php';
?>

        <footer class="footer">
            <div class="container">
                <span class="text-muted">&copy;<?php echo date('Y'); ?> National Elections Commission, Republic of Liberia.</span>
            </div>
        </footer>

        <!--Google recaptch-->
        <script src='https://www.google.com/recaptcha/api.js'></script>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="app/public/js/popper.min.js"></script>
        <script src="app/public/js/bootstrap.min.js"></script>

    </body>
</html>
