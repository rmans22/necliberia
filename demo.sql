# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 192.168.33.10 (MySQL 5.5.57-0ubuntu0.14.04.1)
# Database: necVoter
# Generation Time: 2017-11-24 14:45:47 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table voter
# ------------------------------------------------------------

DROP TABLE IF EXISTS `voter`;

CREATE TABLE `voter` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `voterId` int(9) DEFAULT NULL,
  `voterName` varchar(50) DEFAULT NULL,
  `county` varchar(7) DEFAULT NULL,
  `magisterialArea` varchar(45) DEFAULT NULL,
  `electoralDistrict` int(2) DEFAULT NULL,
  `votingPrecinct` varchar(60) DEFAULT NULL,
  `pollingPlace` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `voter` WRITE;
/*!40000 ALTER TABLE `voter` DISABLE KEYS */;

INSERT INTO `voter` (`id`, `voterId`, `voterName`, `county`, `magisterialArea`, `electoralDistrict`, `votingPrecinct`, `pollingPlace`)
VALUES
	(1,123456789,'JOHN DOE, Male','Montser','Montserrado (Lower)',14,'30068, Mac Foundation School, COW FACTORY',3),
	(2,123456783,'Mary DOE, Female','Montser','Montserrado (Lower)',14,'30068, Mac Foundation School, COW FACTORY',3),
	(3,726738037,'Popei Dennis S, Male','Margibi','Margibi',1,'24106, Rock Instiute School, Rock Church Community',1);

/*!40000 ALTER TABLE `voter` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
